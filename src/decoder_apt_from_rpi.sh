#!/bin/bash

#
#
#


# Main entry point
#echo "# Extinction du bias-tee"
#/home/pi/rtl_biast/build/src/rtl_biast -b 0


echo "## Setting paths ..."
satnogs_data_path=/tmp/.satnogs/data
recorded_files_path=/home/pi/noaa_decoder/data/recorded
#echo "DBG : satnogs_data_path    = ${satnogs_data_path}"
#echo "DBG : recorded_files_path  = ${recorded_files_path}"


echo "## Moving to satnogs data ..."
cd "${satnogs_data_path}"


echo "## Récupération du nom du fichier *.out"
out_file="$(ls -A ./*.out)"
#echo "DBG : out_file       = ${out_file}"

echo "## Création des noms de fichier pour la suite ..."
filename=$(basename -- "${out_file}")
extension="${filename##*.}"
filename="${filename%.*}"

echo "## Remplacement de 'receiving_satnogs' par 'data' ..."
filename="${filename/receiving_satnogs/data}"
#echo "DBG : filename       = ${filename}"


echo "## Copie du fichier *.out en *.ogg"
recorded_file="${filename}.ogg"
#echo "DBG : recorded_file  = ${recorded_file}"
for i in {1..10} ; do
    echo "## Try #${i}"
    if [ -f "${out_file}" ]
      cp "${out_file}" "${recorded_files_path}/${recorded_file}"
    then
      echo "ERROR : File '${out_file}' does not exist"
    fi

    if [ -n "$(ls -A ${recorded_files_path} 2>/dev/null)" ]
    then
      echo "## Destination folder contains *.ogg files"
      break
    fi
    sleep 1
done


echo "## Récupération des paramètres"
#{{FREQ}}        - Observation Centre Frequency
#{{TLE}}         - Target object TLE data, as a JSON-formatted object.
#{{TIMESTAMP}}   - Start time of the observation, in the format: %Y-%m-%dT%H-%M-%S%z
#{{ID}}          - Observation ID number.
#{{BAUD}}        - Baud rate of the modulation used, if applicable.
#{{SCRIPT_NAME}} -

id="${1:-}"
tle="${2:-}"
echo "DBG : id        = ${id}"
echo "DBG : tle       = ${tle}"


echo "## Export du PYTHONPATH"
export PYTHONPATH=${PYTHONPATH}:/home/pi
echo "PYTHONPATH = ${PYTHONPATH}"

echo "## Suppression des fichiers de log"
rm /home/pi/noaa_decoder/logs/*.log

echo "## Execution du décodage NOAA"
cd /home/pi/noaa_decoder/src
./noaa_decoder_main.py --platform "rpi" --stations 793 --observations "${id}" --tle "${tle}" -e class HVC-precip MCIR-precip  #> /tmp/.satnogs/data/test.log 2>&1


cd -
echo "# Fin du script"
