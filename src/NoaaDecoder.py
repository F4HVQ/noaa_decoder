#!/usr/bin/python3.7

""" TODO

"""

# Global imports
import os
import shutil
import sys
import urllib.request
import time
from datetime import datetime, timedelta, date
from PIL import Image
from bs4 import BeautifulSoup
import pprint


# Specific imports
try:
    from noaa_decoder.src import satnogs_api_client
    from libs.Logger import Logger
    from libs.Constants.Constants import Constants
    from libs.Utils import Utils

except ImportError as err:
    print("[IMPORT ERROR]\t\t{} : {}".format(__file__, err))
    sys.exit()


class NoaaDecoder(object):
    """ Class NoaaDecoder
    """

    # Define data directories path
    __PATHS = dict()
    __PATHS['DATA']        = "../data"
    __PATHS['IMAGES']      = os.path.join(__PATHS['DATA'], "images")
    __PATHS['LOGS']        = "../logs"
    __PATHS['MAPS']        = os.path.join(__PATHS['DATA'], "maps")
    __PATHS['RECORDED']    = os.path.join(__PATHS['DATA'], "recorded")
    __PATHS['RESAMPLED']   = os.path.join(__PATHS['DATA'], "resampled")
    __PATHS['TMP_SATNOGS'] = "/tmp/.satnogs/data"
    __PATHS['THUMBNAILS']  = os.path.join(__PATHS['DATA'], "thumbnails")
    __PATHS['TLE']         = os.path.join(__PATHS['DATA'], "tle")
    __PATHS['WEB']         = os.path.join(__PATHS['DATA'], "web")
    __PATHS['WEB_JS']      = os.path.join(__PATHS['WEB'], "resources", "js")
    __PATHS['WEB_HTML']    = os.path.join(__PATHS['WEB'], "resources", "html")

    def __init__(self,
                 which_platform,
                 observations_id,
                 observations_range,
                 stations_id,
                 enhancements,
                 display_map,
                 tle_used,
                 observations_period):
        # type: (str, str, str, str, dict, bool, str, str) -> None
        """ Constructor

        :param which_platform:
        :param observations_id:
        :param observations_range:
        :param stations_id:
        :param enhancements:
        :param display_map:
        :param tle_used:
        :param observations_period:
        """

        self._noaa_norad_ids = [25338, 28654, 33591]  # NOAA satellites

        self._which_platform     = which_platform
        self._observations_id    = observations_id
        self._stations_id        = stations_id
        self._observations_range = observations_range

        if "ALL" in enhancements:
            self._enhancements_list = ["class", "ZA", "MSA", "MSA-precip", "MCIR", "MCIR-precip", "HVC", "HVCT", "HVC-precip", "HVCT-precip"]
        else:
            self._enhancements_list = enhancements

        self._display_map         = display_map
        self._tle_used            = tle_used
        self._observations_period = observations_period

        self._observations_data = None
        self._observation_data  = None
        self._satellite_data    = None
        self._tle_data          = None
        self._station_data      = None

        self._image_infos = {}

    def clean(self, nb_of_day):
        # type: (int) -> None
        """ Delete old files

        :param nb_of_day: Remove files older then nb_of_day
        :return: None
        """

        paths_to_clean = [self.__PATHS['IMAGES'],
                          # self.__PATHS['LOGS'],
                          self.__PATHS['MAPS'],
                          self.__PATHS['RECORDED'],
                          self.__PATHS['RESAMPLED'],
                          self.__PATHS['THUMBNAILS'],
                          self.__PATHS['TLE']
                          ]

        now = time.time()

        for path in paths_to_clean:
            Logger.noaa_decoder_logger.info("Removing old files in '{}'...".format(path))

            for f in os.listdir(path):
                file_path = os.path.join(path, f)
                if os.path.isfile(file_path) and (not f.startswith(".keep")):
                    if nb_of_day < 0:
                        Logger.noaa_decoder_logger.warning("Removing '{}' ...".format(file_path))
                        os.remove(file_path)
                    elif os.stat(file_path).st_mtime < (now - (nb_of_day * 86400)):
                        Logger.noaa_decoder_logger.warning("Removing '{}' ...".format(file_path))
                        os.remove(file_path)

        Logger.noaa_decoder_logger.ok("Removing old files completed")

    def decode(self):
        # type() -> None
        """ TODO DOC

        """

        if "rpi" in self._which_platform:
            self.decode_rpi()
        elif "desktop" in self._which_platform:
            self.decode_desktop()
        else:
            Logger.noaa_decoder_logger.error("Invalid platform")

    def decode_rpi(self):
        # type () -> bool
        """ Full process executed in "rpi mode"

        :return: Boolean
        """

        if not satnogs_api_client.is_satnogs_server_available():
            Logger.noaa_decoder_logger.error("Unable to connect server")
            return Constants.EXIT_FAILURE
        else:
            self.clean(1)
            self.__PATHS['IMAGES'] = self.__PATHS['TMP_SATNOGS']

            self._observation_data = satnogs_api_client.fetch_observation_data(self._observations_id)[0]
            if self._observation_data['transmitter_mode'] in ["APT"]:

                self._station_data     = satnogs_api_client.fetch_ground_station_data(self._stations_id)[0]
                self._satellite_data   = satnogs_api_client.fetch_satellite_data(self._observation_data['norad_cat_id'])
                self._tle_data         = satnogs_api_client.get_tle_of_observation(self._tle_used)
                self.save_tle_data()

                self.resample_observation_audio_file()
                self.touch_observation_audio_file()
                self.generate_map_file()
                self.decode_observation_audio_file(self._display_map)

                return Constants.EXIT_SUCCESS
            else:
                return Constants.EXIT_FAILURE

    def decode_desktop(self):
        # type () -> bool
        """ Full process executed in "desktop mode"

        :return: Boolean
        """

        if not satnogs_api_client.is_satnogs_server_available():
            Logger.noaa_decoder_logger.error("Unable to connect server")
            return Constants.EXIT_FAILURE
        else:
            self.clean(7)
            self.update_index_web_page()

            stations_data = satnogs_api_client.fetch_ground_station_data(self._stations_id)
            for idx, s in enumerate(stations_data):
                self._station_data = s

                Logger.noaa_decoder_logger.info("Station #{} {}/{}".format(self._station_data[0]['id'], (idx + 1), len(stations_data)))

                self.update_station_web_page()
                self.get_observations_data()

                for idx, observation in enumerate(self._observations_data):
                    self._observation_data = observation

                    Logger.noaa_decoder_logger.reinit_indent()
                    Logger.noaa_decoder_logger.info("Observation #{} {}/{}".format(self._observation_data['id'], (idx + 1), len(self._observations_data)))

                    if satnogs_api_client.has_audio_url(self._observation_data):

                        self._satellite_data = satnogs_api_client.fetch_satellite_data(self._observation_data['norad_cat_id'])
                        self._tle_data       = satnogs_api_client.fetch_tle_of_observation(self._observation_data['id'])
                        self.save_tle_data()

                        self.download_observation_audio_file()
                        self.resample_observation_audio_file()
                        self.touch_observation_audio_file()
                        self.generate_map_file()
                        self.decode_observation_audio_file(self._display_map)
                    else:
                        Logger.noaa_decoder_logger.warning("Observation #{} does not have audio file".format(self._observation_data['id']))

            Logger.noaa_decoder_logger.warning("To view decoding result, type the following command :\nfile://{}".format(os.path.abspath(os.path.join(self.__PATHS['WEB'], "index.html"))))
            return Constants.EXIT_SUCCESS

    def get_observations_data(self):
        # type: () -> None
        """

        :return: None
        """

        if self._observations_range is not None:
            observations_start_range = datetime.strptime(self._observations_range[0], "%Y-%m-%d")
            observations_end_range   = datetime.strptime(self._observations_range[1], "%Y-%m-%d")

            self._observations_data = satnogs_api_client.fetch_observation_data_from_id(self._noaa_norad_ids,
                                                                                        observations_start_range,
                                                                                        observations_end_range)

        elif self._observations_period is not None:
            today = date.today()
            observations_start_range = None
            observations_end_range   = None

            if self._observations_period == "today":
                next_day = str(today + timedelta(days=1))

                observations_start_range = datetime.strptime(str(today), "%Y-%m-%d")
                observations_end_range   = datetime.strptime(next_day, "%Y-%m-%d")

                # self._observations_data = satnogs_api_client.fetch_observation_data_from_id(self._noaa_norad_ids,
                #                                                                             observations_start_range,
                #                                                                             observations_end_range)
            elif self._observations_period == "yesterday":
                last_day = str(today - timedelta(days=1))

                observations_start_range = datetime.strptime(last_day, "%Y-%m-%d")
                observations_end_range   = datetime.strptime(str(today), "%Y-%m-%d")

                # self._observations_data = satnogs_api_client.fetch_observation_data_from_id(self._noaa_norad_ids,
                #                                                                             observations_start_range,
                #                                                                             observations_end_range)
            elif self._observations_period == "week":
                last_weekday = str(today - timedelta(days=7))
                next_day     = str(today + timedelta(days=1))

                observations_start_range = datetime.strptime(last_weekday, "%Y-%m-%d")
                observations_end_range   = datetime.strptime(next_day, "%Y-%m-%d")

                # self._observations_data = satnogs_api_client.fetch_observation_data_from_id(self._noaa_norad_ids,
                #                                                                             observations_start_range,
                #                                                                             observations_end_range)

            # Logger.noaa_decoder_logger.debug("observations_start_range : '{}'".format(observations_start_range))
            # Logger.noaa_decoder_logger.debug("observations_end_range   : '{}'".format(observations_end_range))
            self._observations_data = satnogs_api_client.fetch_observation_data_from_id(self._noaa_norad_ids,
                                                                                        observations_start_range,
                                                                                        observations_end_range)

        elif self._observations_id is not None:
            self._observations_data = satnogs_api_client.fetch_observation_data(self._observations_id)

        Logger.noaa_decoder_logger.info("Cleaning and sorting unwanted observations ...")
        tmp_0 = self._observations_data
        tmp_1 = list(filter(lambda i: i['ground_station'] == self._station_data[0]['id'], tmp_0))
        tmp_2 = list(filter(lambda i: i['status'] in ["good", "unknown"], tmp_1))

        self._observations_data = tmp_2

        # Sort by observation id
        self._observations_data.sort(key=lambda k: k['id'], reverse=True)

    @staticmethod
    def __download_file(file_url, destination_path, overwrite):
        # type (str, str, bool) -> bool
        """

        :param file_url:
        :param destination_path:
        :param overwrite: boolean
        :return: bool: EXIT_SUCCESS if download complete else EXIT_FAILURE
        """

        filename  = file_url[file_url.rfind("/") + 1:]
        localpath = os.path.join(destination_path, filename)

        Logger.noaa_decoder_logger.info("Downloading file '{}' ...".format(file_url))
        if not (os.path.exists(localpath) and not overwrite):
            try:
                response = urllib.request.urlretrieve(file_url, localpath)
                if response:
                    Logger.noaa_decoder_logger.ok("Downloading file completed")
                    return Constants.EXIT_SUCCESS

            except urllib.error.URLError as err:
                Logger.noaa_decoder_logger.error("Fail to download file ; {}".format(err))
                return Constants.EXIT_FAILURE
        else:
            Logger.noaa_decoder_logger.warning("File '{}' already exists AND we DO NOT want to overwrite it. So DONT download it.".format(localpath))
            return Constants.EXIT_SUCCESS

    def download_observation_audio_file(self):
        # type () -> bool
        """

        :return: bool: EXIT_SUCCESS if download complete else EXIT_FAILURE
        """

        Logger.noaa_decoder_logger.info("Downloading observation audio file ...")

        observation_audio_url      = satnogs_api_client.get_audio_url(self._observation_data)
        observation_audio_filename = observation_audio_url[observation_audio_url.rfind("/") + 1:]
        filename, ext              = os.path.splitext(observation_audio_filename)
        ret = Constants.EXIT_FAILURE

        self._image_infos['resampled_audio_file_path'] = "{}_11025.wav".format(os.path.join(self.__PATHS['RESAMPLED'], filename))
        if not os.path.exists(self._image_infos['resampled_audio_file_path']):
            ret = self.__download_file(observation_audio_url, self.__PATHS['RECORDED'], False)
            Logger.noaa_decoder_logger.info("Downloading observation audio file completed")

        else:
            Logger.noaa_decoder_logger.warning("Resampled file '{}' already exists. So DO NOT download it".format(self._image_infos['resampled_audio_file_path']))
            ret = Constants.EXIT_SUCCESS
        return ret


    def resample_observation_audio_file(self):
        # type () -> None
        """

        :return:
        """

        Logger.noaa_decoder_logger.info("Resampling audio file ...")

        observation_audio_url      = satnogs_api_client.get_audio_url(self._observation_data)
        observation_audio_filename = observation_audio_url[observation_audio_url.rfind("/") + 1:]
        filename, ext              = os.path.splitext(observation_audio_filename)

        self._image_infos['recorded_audio_file_path']  = os.path.join(self.__PATHS['RECORDED'], observation_audio_filename)
        if os.path.exists(self._image_infos['recorded_audio_file_path']):

            self._image_infos['resampled_audio_file_path'] = "{}_11025.wav".format(os.path.join(self.__PATHS['RESAMPLED'], filename))
            if not os.path.exists(self._image_infos['resampled_audio_file_path']):
                resample_cmd = "sox {} {} rate 11025".format(self._image_infos['recorded_audio_file_path'],
                                                             self._image_infos['resampled_audio_file_path'])
                # Logger.noaa_decoder_logger.debug(resample_cmd)
                Utils.console(resample_cmd)
                Logger.noaa_decoder_logger.ok("Resampling audio file completed")
            else:
                Logger.noaa_decoder_logger.warning("Resampled audio file '{}' already exists; So will not be resampled".format(self._image_infos['resampled_audio_file_path']))
        else:
            Logger.noaa_decoder_logger.warning("Recorded audio file '{}' does not exist.".format(self._image_infos['recorded_audio_file_path']))

    def touch_observation_audio_file(self):
        # type () -> None
        """

        :return:
        """

        if os.path.exists(self._image_infos['resampled_audio_file_path']):
            self.__touch_file(self._image_infos['resampled_audio_file_path'], satnogs_api_client.get_pass_timestamp(self._observation_data, 2))
        else:
            Logger.noaa_decoder_logger.warning("File '{}' do not exists; So will not be touch".format(self._image_infos['resampled_audio_file_path']))

    def generate_map_file(self):
        # type () -> None
        """ TODO

        :return: TODO
        """

        observation_audio_url      = satnogs_api_client.get_audio_url(self._observation_data)
        observation_audio_filename = observation_audio_url[observation_audio_url.rfind("/") + 1:]
        filename, ext              = os.path.splitext(observation_audio_filename)

        self._image_infos['map_file_path'] = os.path.join(self.__PATHS['MAPS'], "{}.map".format(filename))

        Logger.noaa_decoder_logger.info("Generating map file '{}' ...".format(self._image_infos['map_file_path']))
        wxmap_cmd = "wxmap -T '{}' -H '{}' -L '{}' '{}' '{}'".format(self._satellite_data[0]['name'],
                                                                     self._image_infos['tle_file_path'],
                                                                     "{}/{}/{}".format(self._station_data[0]['lat'], self._station_data[0]['lng'], self._station_data[0]['altitude']),
                                                                     satnogs_api_client.get_pass_timestamp(self._observation_data, 2).strftime("%d %m %Y %H:%M"),
                                                                     self._image_infos['map_file_path'])
        # Logger.noaa_decoder_logger.debug(wxmap_cmd)
        Utils.console(wxmap_cmd)

        Logger.noaa_decoder_logger.ok("Generating map file completed")

    def decode_observation_audio_file(self, display_map):
        # type (bool) -> None
        """ TODO

        :param display_map : TODO
        :return: None
        """

        observation_audio_url      = satnogs_api_client.get_audio_url(self._observation_data)
        observation_audio_filename = observation_audio_url[observation_audio_url.rfind("/") + 1:]
        filename, ext              = os.path.splitext(observation_audio_filename)

        Logger.noaa_decoder_logger.info("Decoding audio file '{}' ...".format(observation_audio_url))
        for e in self._enhancements_list:
            Logger.noaa_decoder_logger.info("With enhancement : '{}' ...".format(e))

            # Determine le sens du passage
            if 90.0 <= self._observation_data['rise_azimuth'] <= 270.0:
                way = "-N"
            elif 90.0 <= self._observation_data['set_azimuth'] <= 270.0:
                way = "-S"
            else:
                way = ""

            if display_map:
                self._image_infos['decoded_image_file_path'] = os.path.join(self.__PATHS['IMAGES'], "{}_{}_map.jpg".format(filename, e))
                self._image_infos['thumbnail_file_path']     = os.path.join(self.__PATHS['THUMBNAILS'], "{}_{}_map_thumb.jpg".format(filename, e))

                wxtoimg_cmd = "wxtoimg -c -k. -t n -e {} -m '{}' -A '{}' '{}'".format(e,
                                                                                         self._image_infos['map_file_path'],
                                                                                         self._image_infos['resampled_audio_file_path'],
                                                                                         self._image_infos['decoded_image_file_path'])
            else:
                self._image_infos['decoded_image_file_path'] = os.path.join(self.__PATHS['IMAGES'], "{}_{}.jpg".format(filename, e))
                self._image_infos['thumbnail_file_path']     = os.path.join(self.__PATHS['THUMBNAILS'], "{}_{}_thumb.jpg".format(filename, e))

                wxtoimg_cmd = "wxtoimg -c -k. -t n {} -e {} -A '{}' '{}'".format(way,
                                                                                    e,
                                                                                    self._image_infos['resampled_audio_file_path'],
                                                                                    self._image_infos['decoded_image_file_path'])
            # Logger.noaa_decoder_logger.debug(wxtoimg_cmd)
            Utils.console(wxtoimg_cmd)

            if "desktop" in self._which_platform:
                self.__create_thumb(self._image_infos['decoded_image_file_path'],
                                    self._image_infos['thumbnail_file_path'],
                                    (720, 720))

                # Changement du chemin relatif
                self._image_infos['decoded_image_file_path'] = os.path.relpath(self._image_infos['decoded_image_file_path'], self.__PATHS['WEB'])
                self._image_infos['thumbnail_file_path']     = os.path.relpath(self._image_infos['thumbnail_file_path'], self.__PATHS['WEB'])

                self.__update_web_page(self._image_infos['decoded_image_file_path'], self._image_infos['thumbnail_file_path'], e)

        Logger.noaa_decoder_logger.ok("Decoding audio file completed")

    def save_tle_data(self):
        # type () -> None
        """ TODO
        :return: TODO
        """

        Logger.noaa_decoder_logger.info("Saving TLE data ...")

        observation_audio_url      = satnogs_api_client.get_audio_url(self._observation_data)
        observation_audio_filename = observation_audio_url[observation_audio_url.rfind("/") + 1:]
        filename, ext              = os.path.splitext(observation_audio_filename)

        self._image_infos['tle_file_path'] = os.path.join(self.__PATHS['TLE'], "{}.tle".format(filename))

        if not os.path.exists(self._image_infos['tle_file_path']):
            with open(self._image_infos['tle_file_path'], mode='wt', encoding='utf-8') as file:
                file.write(self._satellite_data[0]['name'])
                file.write("\n")
                file.write(self._tle_data[0])
                file.write("\n")
                file.write(self._tle_data[1])
            Logger.noaa_decoder_logger.ok("Downloading TLE data completed")

        else:
            Logger.noaa_decoder_logger.warning("File '{}' already exists. So DO NOT download it.".format(self._image_infos['tle_file_path']))

    @staticmethod
    def __touch_file(file_path, timestamp):
        # type(str, str) -> None
        """ TODO

        :param file_path: str
        :return: None
        """

        Logger.noaa_decoder_logger.info("Setting timestamp '{}' to file '{}' ...".format(timestamp, file_path))
        touch_cmd = "touch -t {} {}".format(timestamp.strftime("%y%m%d%H%M"), file_path)
        # Logger.noaa_decoder_logger.debug(touch_cmd)
        Utils.console(touch_cmd)
        Logger.noaa_decoder_logger.ok("Setting file timestamp completed")

    def remove_observation_audio_file(self):
        # type () -> None
        """

        :return: None
        """

        Logger.noaa_decoder_logger.info("Removing observation audio file '{}' ...".format(self._image_infos['recorded_audio_file_path']))

        if os.path.exists(self._image_infos['recorded_audio_file_path']):
            os.remove(self._image_infos['recorded_audio_file_path'])
            Logger.noaa_decoder_logger.ok("Removing observation audio file completed".format(self._image_infos['recorded_audio_file_path']))
        else:
            Logger.noaa_decoder_logger.error("File '{}' does NOT exist. Impossible to delete it".format(self._image_infos['recorded_audio_file_path']))

    def __update_web_page(self, decoded_image_path, thumb_image_path, enhancement):
        # type (str, str) -> None
        """ TODO

        :param decoded_image_path: TODO
        :param thumb_image_path: TODO
        :param enhancement: TODO
        :return: TODO
        """

        Logger.noaa_decoder_logger.info("Updating web page ...")
        with open(file=os.path.join(self.__PATHS['WEB'], "wxtoimg_{}.js".format(self._station_data[0]['id'])), mode='a') as js_file:
            js_file.write("addImg('{}','{}','{}','{}','{}','{}','{}');".format(decoded_image_path,
                                                                               thumb_image_path,
                                                                               self._observation_data['id'], satnogs_api_client.get_pass_timestamp(self._observation_data, 2),
                                                                               enhancement,
                                                                               self._satellite_data[0]['name'], "{} MHz".format(self._observation_data['transmitter_downlink_low'] / 1000000.0)))
            js_file.write("\n")

            Logger.noaa_decoder_logger.ok("Updating web page completed")

    def update_index_web_page(self):
        # type: () -> None
        """

        :return: None
        """

        # stations_list = ""
        # for s in self._stations_id:
        #     stations_list += '\n\t<li><a href="station_{0}.html">Station {0}</a></li>'.format(s)

        with open(os.path.join(self.__PATHS['WEB_HTML'], "orig_index.html"), 'r') as html_page:
            soup = BeautifulSoup(html_page.read(), "html.parser")
            ul_tag = soup.find("div", {"class": "stations_list"}).find("ul")
            for s in self._stations_id:
                new_li_tag = soup.new_tag('li')
                new_href_tag = soup.new_tag("a", href="station_{}.html".format(s))
                new_href_tag.string = "Station {}".format(s)
                new_li_tag.append(new_href_tag)
                ul_tag.append(new_li_tag)

            with open(os.path.join(self.__PATHS['WEB'], "index.html"), 'w') as html_new_page:
                html_new_page.write(str(soup))

    def update_station_web_page(self):
        # type: () -> None
        """

        :return: None
        """

        # Copy original files to clear old file
        shutil.copy(os.path.join(self.__PATHS['WEB_JS'], "orig_wxtoimg.js"),
                    os.path.join(self.__PATHS['WEB'], "wxtoimg_{}.js".format(self._station_data[0]['id'])))

        txt = """
        Station : {} (#{}) ;
        QTH locator : {} ;
        Latitude: {}, Longitude: {}, Altitude: {} m ;
        {}""".format(self._station_data[0]['name'],
                     self._station_data[0]['id'],
                     self._station_data[0]['qthlocator'],
                     self._station_data[0]['lat'],
                     self._station_data[0]['lng'],
                     self._station_data[0]['altitude'],
                     self._station_data[0]['description'])

        with open(os.path.join(self.__PATHS['WEB_HTML'], "orig_station_id.html"), 'r') as html_page:
            soup       = BeautifulSoup(html_page.read(), "html.parser")
            elt        = soup.find("div", {"class": "wxflag"}).find("h4")
            elt.string = txt

            elt = soup.find("div", {"class": "wxdesc"})
            elt.string = "Description de la commande :" \
                         "   Observation id     : {}" \
                         " ; Observation range  : {}" \
                         " ; Observation period : {}" \
                         " ; Enhancement list   : {}" \
                         " ; Display map        : {}".format(self._observations_id,
                                                             self._observations_range,
                                                             self._observations_period,
                                                             self._enhancements_list,
                                                             self._display_map)

            elt = soup.find("div", {"id": "wximages"})
            new_script_tag = soup.new_tag("script", type="text/javascript", src="./wxtoimg_{}.js".format(self._station_data[0]['id']))
            elt.append(new_script_tag)

            with open(os.path.join(self.__PATHS['WEB'], "station_{}.html".format(self._station_data[0]['id'])), 'w') as html_new_page:
                html_new_page.write(str(soup))

    @staticmethod
    def __create_thumb(decoded_image_path, thumb_image_path, size):
        # type (TODO, TODO, TODO) -> None
        """ TODO

        :param decoded_image_path: TODO
        :param thumb_image_path: TODO
        :param size: TODO
        :return: TODO
        """

        im = Image.open(decoded_image_path)
        im.thumbnail(size)
        im.save(thumb_image_path)
