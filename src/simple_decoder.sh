#!/bin/bash

#
#
#


## Functions definition
function stop_biastee() {
  echo "# Extinction du bias-tee"
  /home/pi/rtl_biast/build/src/rtl_biast -b 0
}

function decode_apt() {
  echo "# Execution du décodage NOAA"
  echo "## Setting paths ..."
  local satnogs_data_path=/tmp/.satnogs/data
  local recorded_files_path=/home/pi/noaa_decoder/data/recorded
  local resampled_files_path=/home/pi/noaa_decoder/data/resampled
  local images_files_path=/home/pi/noaa_decoder/data/images
#  echo "DBG : satnogs_data_path    = ${satnogs_data_path}"
#  echo "DBG : recorded_files_path  = ${recorded_files_path}"
#  echo "DBG : resampled_files_path = ${resampled_files_path}"
#  echo "DBG : images_files_path    = ${images_files_path}"


  echo "## Suppression des fichiers temporaires ..."
  rm ${recorded_files_path}/*
  rm ${resampled_files_path}/*
  rm ${images_files_path}/*

  echo "## Moving to satnogs data ..."
  cd "${satnogs_data_path}"


  echo "## Récupération du nom du fichier *.out"
  local out_file="$(ls -A ./*.out)"
#  echo "DBG : out_file       = ${out_file}"

  echo "## Création des noms de fichier pour la suite ..."
  local filename=$(basename -- "${out_file}")
  local extension="${filename##*.}"
  filename="${filename%.*}"

  echo "## Remplacement de 'receiving_satnogs' par 'data' ..."
  filename="${filename/receiving_satnogs/data}"
#  echo "DBG : filename       = ${filename}"


  echo "## Copie du fichier *.out en *.ogg"
  local recorded_file="${filename}.ogg"
#  echo "DBG : recorded_file  = ${recorded_file}"
  for i in {1..10} ; do
      echo "## Try #${i}"
      if [ -f "${out_file}" ]
        cp "${out_file}" "${recorded_files_path}/${recorded_file}"
      then
        echo "ERROR : File '${out_file}' does not exist"
      fi

      if [ -n "$(ls -A ${recorded_files_path} 2>/dev/null)" ]
      then
        echo "## Destination folder contains *.ogg files"
        break
      fi
      sleep 1
  done

  echo "## Reechantillonage ..."
  local resampled_file="${filename}_11025.wav"
#  echo "DBG : resampled_file = ${resampled_file}"
  sox "${recorded_files_path}/${recorded_file}" "${resampled_files_path}/${resampled_file}" rate 11025


  echo "## Decodage de l'image ..."
  local image_file=""

  image_file="${filename}_class.png"
#  echo "DBG : image_file     = ${image_file}"
  wxtoimg -N -c -k. -t n -e "class" -A "${resampled_files_path}/${resampled_file}" "${images_files_path}/${image_file}"

  image_file="${filename}_hvc-precip.png"
#  echo "DBG : image_file     = ${image_file}"
  wxtoimg -N -c -k. -t n -e "HVC-precip" -A "${resampled_files_path}/${resampled_file}" "${images_files_path}/${image_file}"

  image_file="${filename}_mcir-precip.png"
#  echo "DBG : image_file     = ${image_file}"
  wxtoimg -N -c -k. -t n -e "MCIR-precip" -A "${resampled_files_path}/${resampled_file}" "${images_files_path}/${image_file}"


  echo "## Moving decoded image to satnogs data folder ..."
  mv "${images_files_path}"/* "${satnogs_data_path}"

}

# Main entry point
stop_biastee
decode_apt

cd -
echo "# Fin du script"
