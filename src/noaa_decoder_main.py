#!/usr/bin/python3.7

"""
TODO
"""

# Global imports
import argparse
import re
import sys
import time
from datetime import datetime

# Specific imports
try:
    from libs.Constants.Constants import Constants
    from libs.Logger import Logger
    from noaa_decoder.src import NoaaDecoder

except ImportError as err:
    print("[IMPORT ERROR]\t\t{} : {}".format(__file__, err))
    sys.exit()


# Functions definition
def check_parameters(args):
    # type () -> bool
    """ Check input parameters """

    Logger.noaa_decoder_logger.info("Checking parameters ...")

    if args.OBSERVATIONS_RANGE is not None:
        pattern = '\d{4}-\d{2}-\d{2}'
        rgx = re.compile(pattern)
        for r in args.OBSERVATIONS_RANGE:
            if not re.match(rgx, r):
                Logger.noaa_decoder_logger.error("Argument '{}' is NOT a DATE pattern for --observations_range option".format(r))
                Logger.noaa_decoder_logger.warning("Pattern must be as follow : '{}'".format(pattern))
                return Constants.EXIT_FAILURE

        start_range = datetime.strptime(args.OBSERVATIONS_RANGE[0], "%Y-%m-%d")
        end_range   = datetime.strptime(args.OBSERVATIONS_RANGE[1], "%Y-%m-%d")
        if end_range < start_range:
            Logger.noaa_decoder_logger.error("Argument '{}' is NOT in timeline order for --observations_range option".format(args.OBSERVATIONS_RANGE))
            Logger.noaa_decoder_logger.warning("Swap observations range values")
            return Constants.EXIT_FAILURE

    if args.ENHANCEMENTS is not None:
        pattern = '[A-Za-z\-]+'
        rgx = re.compile(pattern)
        for r in args.ENHANCEMENTS:
            if not re.match(rgx, r):
                Logger.noaa_decoder_logger.error("Argument '{}' is NOT a ENHANCEMENTS pattern for --enhancements option".format(r))
                Logger.noaa_decoder_logger.warning("Pattern must be as follow : '{}'".format(pattern))
                return Constants.EXIT_FAILURE

    Logger.noaa_decoder_logger.ok("Checking parameters completed")


if __name__ == "__main__":

    # Logger initialisation
    # Logger.noaa_decoder_logger.clear()
    Logger.noaa_decoder_logger.info("=== {} ===".format(time.strftime("%c")))
    Logger.noaa_decoder_logger.info("Script log from '{}' saved in '{}' directory".format(sys.argv[0], "./logs"))

    # Specify command arguments
    epilog_msg = '''\
    See above examples :
        - noaa_decoder.py -s STATION_ID -o ID_1 ID_n -e ENHANCEMENT_1 ENHANCEMENT_n
        - noaa_decoder.py -s STATION_ID -r "YYYY-MM-DD" "YYYY-MM-DD" -e ENHANCEMENT_1 ENHANCEMENT_n
        - noaa_decoder.py -s STATION_ID -p today -e ENHANCEMENT_1 ENHANCEMENT_n
        - noaa_decoder.py -s STATION_ID -p week -e ENHANCEMENT_1 ENHANCEMENT_n
        
        - noaa_decoder.py --stations STATION_ID--observation ID_1 ID_n --enhancements ENHANCEMENT_1 ENHANCEMENT_n
        - noaa_decoder.py --stations STATION_ID --observations_range "YYYY-MM-DD" "YYYY-MM-DD" --enhancements ENHANCEMENT_1 ENHANCEMENT_n
        
        
    Here is the list of enhancements:
        class ZA MSA MSA-precip MCIR MCIR-precip HVC HVCT HVC-precip HVCT-precip
    '''

    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description="This script execute preliminary processes in order to decode NOAA images from satnogs website",
                                     epilog=epilog_msg)

    group_options = parser.add_argument_group('options')

    group_options.add_argument('-e', '--enhancements',
                               nargs='+',
                               dest='ENHANCEMENTS',
                               choices=["ALL", "class", "ZA", "MSA", "MSA-precip", "MCIR", "MCIR-precip", "HVC", "HVCT", "HVC-precip", "HVCT-precip"],
                               action='store',
                               default="class",
                               required=False,
                               help='The list of enhancements')

    group_options.add_argument('-m', '--map',
                               dest='DISLPLAY_MAP',
                               action='store_true',
                               default=False,
                               required=False,
                               help='Display map')

    group_options.add_argument('-o', '--observations',
                               nargs='+',
                               dest='OBSERVATIONS_ID',
                               type=int,
                               action='store',
                               default=None,
                               required=False,
                               help='Observations ID')

    group_options.add_argument('-p', '--observations_period',
                               dest='OBSERVATIONS_PERIOD',
                               choices=["today", "week", "yesterday"],
                               action='store',
                               default=None,
                               required=False,
                               help='The observations period')

    group_options.add_argument('-r', '--observations_range',
                               nargs=2,
                               dest='OBSERVATIONS_RANGE',
                               action='store',
                               default=None,
                               required=False,
                               help='The observations range')

    group_options.add_argument('-s', '--stations',
                               nargs='+',
                               dest='STATIONS_ID',
                               type=int,
                               action='store',
                               default=None,
                               required=True,
                               help='Stations id to decode')

    group_options.add_argument('-t', '--tle',
                               dest='TLE_USED',
                               type=str,
                               action='store',
                               default=None,
                               required=False,
                               help='The tle used for pass')

    group_options.add_argument('-T', '--timestamp',
                               dest='TIMESTAMP',
                               type=str,
                               action='store',
                               default=None,
                               required=False,
                               help='The timestamp of the current pass')

    group_options.add_argument('-P', '--platform',
                               dest='WHICH_PLATFORM',
                               choices=["rpi", "desktop"],
                               type=str,
                               action='store',
                               default=None,
                               required=True,
                               help='The origin platform')

    args = parser.parse_args()
    if Constants.EXIT_FAILURE == check_parameters(args):
        exit(Constants.EXIT_FAILURE)
    else:
        noaa_decoder = NoaaDecoder.NoaaDecoder(
                                                args.WHICH_PLATFORM,
                                                args.OBSERVATIONS_ID,
                                                args.OBSERVATIONS_RANGE,
                                                args.STATIONS_ID,
                                                args.ENHANCEMENTS,
                                                args.DISLPLAY_MAP,
                                                args.TLE_USED,
                                                args.OBSERVATIONS_PERIOD)

        noaa_decoder.decode()
