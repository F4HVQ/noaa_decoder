var xlang = new Array();

// -----------------------------------------------------
// language transation for satellite info here.
// change only right-hand side, it must be surrounded
// by double quotes and end with a semicolon.
// You can add other projection translations as needed.
// You can also translate other enhancement names.
xlang['click']		     = "click image for full-size image";
xlang['on']		         = "on";
xlang['at']		         = "at";
xlang['Pass Start']      = "Pass Start";
xlang['Enhancement']	 = "Enhancement";
xlang['Frequency']	     = "Frequency";
xlang['Link']	         = "Link";

// -----------------------------------------------------

function xlate(s) {
    if (xlang[s] == undefined || xlang[s] == "")
        return s;
    return xlang[s];
}

function addImg(img, thumb, id, time, enh, sat, freq) {

    at      = ' ' + xlate('at') + ' ';
    on      = ' ' + xlate('on') + ' ';

    title = sat + at + time;
    info  = sat + ' ' + on + freq + ', ' + enh;

    document.write('<div class="wximage" style="height: ' + 488 + 'px;">');
    document.write('    <span class="wximagetitle"><h3>' + sat + '</h3></span>');
    document.write('    <a href="' + img + '"><img src="' + thumb + '" alt="' + '" title="' + xlate('click') + '" width="275" height="381" style="border: 0px;"' + '"></a>');
    document.write('    <table class="wxinfo">');
    document.write('        <tr><td>' + xlate('Link')         	+ ':</td><td>' + '<a target="_blank" href="https://network.satnogs.org/observations/' + id + '/">' + id + '</a>' + '</td></tr>');
    document.write('        <tr><td>' + xlate('Pass Start')		+ ':</td><td>' + time       + '</td></tr>');
    document.write('        <tr><td>' + xlate('Enhancement')	+ ':</td><td>' + xlate(enh) + '</td></tr>');
    document.write('        <tr><td>' + xlate('Frequency')		+ ':</td><td>' + freq       + '</td></tr>');
    document.write('    </table>');
    document.write('</div>');
}

