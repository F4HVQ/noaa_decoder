Avec un PC linux et quelques manipulations je penses que c'est possible.

Voici la procédure. La plupart des actions se feront en lignes de commandes:

# Procédure

## Légende :
[?]$ 			Action exécutée en tant qu'utilisateur standard, depuis n'importe quel endroit
[Downloads]#	Action exécutée en tant qu'utilisateur root (privilèges élevés), depuis le dossier "Downloads"

## Récupération de l'archive
1) Récupérer l'archive "SATNOGS_20200614_pour_f4dwd.tar.xz" ci-jointe. La placer dans le dossier ~/Downloads
1) Ouvrir un terminal (fenêtre noire ;))


## Installation de Python 3.7 et des librairies nécessaires
[?]$ su
[~]# apt update
[~]# apt upgrade
[~]# apt install python3.7
[~]# apt install python3-pip
[~]# python3.7 -m pip install Pillow
[~]# python3.7 -m pip install Pillow-PIL
[~]# python3.7 -m pip install requests
[~]# python3.7 -m pip install beautifulsoup4
[~]# exit

## Création du dossier dans lequel sera stocké le code (Il s'agit du même endroit au club ou chez moi)
[?]$ mkdir ~/Documents/codings

## 
[?]$ cd ~/Downloads
[Downloads]$ tar -xvf SATNOGS_20200614_pour_f4dwd.tar.xz
[Downloads]$ cp -r ./SATNOGS_20200614_pour_f4dwd ~/Documents/codings/SATNOGS

## Utilisation basique
[Downloads]$ cd ~/Documents/codings/SATNOGS/noaa_decoder/src
[src]$ ./noaa_decoder_main.py
[src]$ ./noaa_decoder_main.py --help

